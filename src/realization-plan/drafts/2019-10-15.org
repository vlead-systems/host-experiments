#+TITLE: Mail to Pushpdeep about hosting process
#+AUTHOR: VLEAD
#+DATE: [2019-10-15 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Draft
Please find at this [[https://gitlab.com/vlead-systems/host-experiments/blob/develop/src/runtime/wf.org][link]] the work flow for hosting.  By
defining the workflow, the interfaces become well defined to
facilitate automation.

This workflow forces to streamline the build process of each
experiment.  It requires to provide an interface to build
each experiment.  What this means is: every experiment
source repo has a makefile whose default target builds the
experiment into the =build= folder at the top level.

The hosting robot invokes this makefile after cloning an
experiment repository and the build folder that is generated
gets hosted.
