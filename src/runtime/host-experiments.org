#+TITLE: Build an Experiment
#+AUTHOR: VLEAD
#+DATE: [2019-10-17 Thu]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
Build an experiment with the build interface provided by IIT
Bombay.

* Design

The hosting process will read through a list of experiments
and perform the following:
1. clone the experiment
2. build the experiment using the build interface to a build
   folder

Once the list of experiments is done, the build folder
containing all the built experiments is hosted.

#+BEGIN_EXAMPLE
host-experiments : exp-list, (build-exp: exp-src-url, build-loc -> built-exp) -> hosted-exp-url-list
#+END_EXAMPLE

* Implementation

** Clone the build infra
The build interface jointly defined by IIITH and IITB is
cloned as infrastructure.  This repo contains the
implementation of the interface =build_exp <url>
<build_loc=.

#+NAME: clone-infra
#+BEGIN_SRC python
def create_path(loc):
    execute_command("mkdir -p %s" % (loc))

def install_infra():
    create_path(INFRA_LOC)
    create_path(BUILT_EXP_LOC)
    fetch_repo(INFRA_REPO, INFRA_BRANCH, INFRA_ID)

def clean_infra():
    remove_infra_cmd = "%s %s" % ("rm -rf", INFRA_LOC)
    try:
        (ret_code, output) = execute_command(remove_infra_cmd)
        print "clean infra successful"
    except Exception, e:
        print "clean infra unsuccessful : " + str(e)
        raise e
    
#+END_SRC

** Build the Experiment
#+NAME: build-exp
#+BEGIN_SRC python
def build_exp(elem):
    script_cmd = "cd %s; ./%s %s %s" % (BUILD_SCRIPT_LOC, BUILD_SCRIPT, elem['exp-url'], BUILT_EXP_LOC)
    try:
        (ret_code, output) = execute_command(script_cmd)
        print "script run successful"
    except Exception, e:
        print "script run unsuccessful : " + str(e)
        raise e

    return elem['exp-url']

#+END_SRC

** Copy built experiments to hosting server
#+NAME: copy-built-exps-to-hosting-server
#+BEGIN_SRC python
def copy_exps_to_server():
    dest_arg = "%s@%s:%s/" % (SERVER_USER, SERVER_IP, EXP_COPY_LOC)
    rsync_cmd = "rsync -a %s/ %s" % (BUILT_EXP_LOC, dest_arg)
    try:
        (ret_code, output) = execute_command(rsync_cmd)
        print "copy to remote successful"
    except Exception, e:
        print "copy to remote unsuccessful : " + str(e)
        raise e

#+END_SRC    

** Host Experiments
#+NAME: host-experiments
#+BEGIN_SRC python
def host_experiments():
    with open(EXPERIMENT_LIST_FILE) as f:
        data = json.load(f)

    result = map(build_exp, data)
    copy_exps_to_server()
    return result

#+END_SRC

** Utils
#+NAME: utils
#+BEGIN_SRC python

def execute_command(cmd):
    print "command: %s" % (cmd)
    return_code = -1
    output = None
    # TODO: Checkout alternative os.system(cmd)
    try:
        output = subprocess.check_output(cmd, shell=True)
        return_code = 0
    except subprocess.CalledProcessError as cpe:
        print "Called Process Error Message: %s" % (cpe.output)
        raise cpe
    except OSError as ose:
        print "OSError: %s" % (ose.output)
        raise ose

    return (return_code, output)

def clone_repo(url, branch, dir_name):
    repo_full_path = get_repo_full_path(dir_name)
    if branch == "":
        branch = "master"

    clone_cmd = "git clone -b %s %s %s" % (branch, url, repo_full_path)

    try:
        (ret_code, output) = execute_command(clone_cmd)
    except Exception, e:
        print "Error Cloning the repository: " + str(e)
        raise e


def pull_repo(url, branch, dir_name):
    repo_full_path = get_repo_full_path(dir_name)
    if branch == "":
        branch = "master"
    pull_cmd = "git -C %s checkout %s;git -C %s pull origin %s" \
      % (repo_full_path, branch, repo_full_path, branch)

    try:
        (ret_code, output) = execute_command(pull_cmd)
        print "Pull repo successful"
    except Exception, e:
        print "Error Pulling the repository: " + str(e)
        raise e


def get_repo_full_path(exp_id):
    repo_path = INFRA_LOC + "/" + exp_id
    return repo_path


def repo_exists(exp_id):
    full_repo_path = get_repo_full_path(exp_id)
    return os.path.isdir(full_repo_path)

def make_infra_path():
    execute_command("mkdir -p %s" % (INFRA_LOC))

def fetch_repo(url, branch, id):
    if repo_exists(id):
        pull_repo(url, branch, id)
    else:
        clone_repo(url, branch, id)

#+END_SRC
** Experiments List
#+NAME: experiment-list
#+BEGIN_SRC json

[
    { "exp-id": "first",
      "exp-name": "First Experiment",
      "exp-url": "http://vlabs.iitb.ac.in/gitlab/Community-Docs/New-Lab-development/Samples/newtons_ring_experiment_sample.git"
    },

    { "exp-id": "second",
      "exp-name": "Second Experiment",
      "exp-url": "http://vlabs.iitb.ac.in/gitlab/Community-Docs/New-Lab-development/Samples/newtons_ring_experiment_sample.git"
    },

    { "exp-id": "third",
      "exp-name": "Third Experiment",
      "exp-url": "http://vlabs.iitb.ac.in/gitlab/Community-Docs/New-Lab-development/Samples/newtons_ring_experiment_sample.git"
    },

    { "exp-id": "four",
      "exp-name": "Fourth Experiment",
      "exp-url": "http://vlabs.iitb.ac.in/gitlab/Community-Docs/New-Lab-development/Samples/newtons_ring_experiment_sample.git"
    },

    { "exp-id": "five",
      "exp-name": "Fifth Experiment",
      "exp-url": "http://vlabs.iitb.ac.in/gitlab/Community-Docs/New-Lab-development/Samples/newtons_ring_experiment_sample.git"
    }
]

#+END_SRC

** Targets
#+NAME: targets
#+BEGIN_SRC python
import sys

from install_infra import *
from  host_experiments import *

def parse_args(*args):
    arg_dict = {}
    pairs = args[0]
    for pair in pairs:
        (key, val) = pair.split('=')
        arg_dict[key] = val

    return arg_dict

if __name__ == '__main__':
    arguments = parse_args(sys.argv[1:])
    target = arguments['target']
    if target == "install-infra":
        install_infra()
    elif target == "host-experiments":
        arr = host_experiments()
        print arr
    elif target == "clean-infra":
        arr = clean_infra()
        print arr

#+END_SRC

** Makefile
#+NAME: makefile
#+BEGIN_SRC makefile
host-experiments:
	(export PYTHONPATH=`pwd`; python targets.py target=host-experiments)

install-infra: 
	(export PYTHONPATH=`pwd`; python targets.py target=install-infra)

all: install-infra host-experiments
	echo "Install the infra and hosts the experiments"

clean-infra:
	(export PYTHONPATH=`pwd`; python targets.py target=clean-infra)
#+END_SRC
* Tangle
** Install Infra
#+BEGIN_SRC python :tangle install_infra.py :eval no :noweb yes
from utils import * 
from config import * 
<<clone-infra>>
#+END_SRC
** Host Experiment
#+BEGIN_SRC json :tangle host_experiments.py :eval no :noweb yes
import json

from config import *
from utils import *

<<build-exp>>
<<copy-built-exps-to-hosting-server>>
<<host-experiments>>
#+END_SRC
** Targets
#+BEGIN_SRC python :tangle targets.py :eval no :noweb yes
<<targets>>
#+END_SRC

** Utilities
#+BEGIN_SRC python :tangle utils.py :eval no :noweb yes
import os
import importlib
import subprocess

from config import *

<<utils>>

#+END_SRC

** Makefile
#+BEGIN_SRC makefile :tangle makefile :eval no :noweb yes
<<makefile>>
#+END_SRC
